var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var errorhandler = require('errorhandler');

var routes = require('./routes/index');
var users = require('./routes/users');

var mongoose = require('mongoose');
var passport = require('passport');

// Importamos el modelo usuario y la configuración de passport
require('./models/user');
require('./passport')(passport);

// Conexión a la base de datos que tenemos en local
mongoose.connect('mongodb://localhost:10000/passport-example', function(err, res){
  if(err) throw err;
  console.log('Conectado con éxito a la BD');
});

// Iniciamos la aplcación Express
var app = express();

app.set('port', process.env.PORT || 5000);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//app.use(express.favicon());
app.use(favicon(__dirname + '/public/favicon.ico'));
// app.use(express.logger('dev'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Indicamos que use sesiones, paa almacenar el objeto usuario
// y que lo recuerde aunque abandonemos la página.
// app.use(express.session({secret: 'lolillo'}));
app.use(session({
  secret: 'lolillo',
  resave: false,
  saveUninitialized: true
}))


// COnfiguración de Passport. La inicializamos
// y le indicamos que Passport maneje su sesión.
app.use(passport.initialize());
app.use(passport.session());
// app.use(app.router);

// Si estoy en local, le indicamos que maneje los errores
// y nos mueste el log más detallado.
if (process.env.NODE_ENV === 'development') {
  // only use in development 
  app.use(errorhandler())
}

/* Rutas de la aplicación */

//app.get('/', routes.index);

/* Rutas de Passport */
// Ruta para desloguearse
/* app.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
}); */

var authRoutes = express.Router();

// Ruta para autenticarse con Twitter (enlace de login)
authRoutes.get('/auth/twitter', function(req, res) {
  res.send ('hola');
});

// Ruta para autenticarse con Facebook (enlace de login)
authRoutes.get('/auth/facebook', passport.authenticate('facebook'));

// Ruta de callback, a la que redirigirá tras autenticarse con Twitter.
// En caso de fallo, redirige a otra vista '/login'.
authRoutes.get('/auth/twitter/callback', passport.authenticate('twitter',
  { succesRedirect: '/', failuredRedirect: '/login'}
));

// Ruta de callback, a la que redirigirá tras autenticarse con Facebook.
// En caso de fallo, redirige a otra vista '/login'.
authRoutes.get('/auth/facebook/callback', passport.authenticate('facebook',
  { succesRedirect: '/', failuredRedirect: '/login'}
));

// Inicia el servidor
app.listen(app.get('port'), function(){
  console.log('Aplicación Express escuchando en el puerto ' + app.get('port'));
});


app.use('/', authRoutes);

app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
